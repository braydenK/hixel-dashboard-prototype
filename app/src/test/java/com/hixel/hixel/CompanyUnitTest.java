package com.hixel.hixel;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CompanyUnitTest {

    @Test
    public void companyHasName() {
        Company c = new Company("Apple", "AAPL", 1000.0f);
        assertEquals("Apple", c.getName());
    }

    @Test
    public void companyHasTicker() {
        Company c = new Company("Apple", "AAPL", 1000.0f);
        assertEquals("AAPL", c.getTicker());
    }

    @Test
    public void companyHasValue() {
        Company c = new Company("Apple", "AAPL", 1000.0f);
        assertEquals(1000.0f, c.getValue(), 0.001);
    }

}
