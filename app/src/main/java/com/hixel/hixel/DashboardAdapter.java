package com.hixel.hixel;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Locale;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.ViewHolder> {
    private ArrayList<Company> mDataset;

    DashboardAdapter(ArrayList<Company> dataset) {
        mDataset = dataset;
    }

    @Override
    public DashboardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.row, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DashboardAdapter.ViewHolder holder, int position) {
        holder.companyName.setText(mDataset.get(position).getName());
        holder.companyTicker.setText(mDataset.get(position).getTicker());
        holder.companyValue.setText(
                String.format(Locale.ENGLISH, "%.2f", mDataset.get(position).getValue()));

        if (mDataset.get(position).getValue() < 1000.0f) {
            holder.companyValue.setTextColor(Color.parseColor("#F44336"));
        } else if (mDataset.get(position).getValue() > 1000.0f) {
            holder.companyValue.setTextColor(Color.parseColor("#4CAF50"));
        } else {
            holder.companyValue.setTextColor(Color.parseColor("#FFC107"));
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView companyName;
        TextView companyTicker;
        TextView companyValue;

        ViewHolder(View itemView) {
            super(itemView);

            companyName = (TextView) itemView.findViewById(R.id.companyName);
            companyTicker = (TextView) itemView.findViewById(R.id.companyTicker);
            companyValue = (TextView) itemView.findViewById(R.id.companyValue);
        }
    }
}
