package com.hixel.hixel;

public class Company {
    private String name;
    private String ticker;
    private float value;

    Company(String name, String ticker, float value) {
        this.name = name;
        this.ticker = ticker;
        this.value = value;
    }

    public String getName() { return name; }

    public String getTicker() { return ticker; }

    public float getValue() { return value; }
}
