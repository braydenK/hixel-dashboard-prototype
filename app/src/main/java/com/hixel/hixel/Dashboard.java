package com.hixel.hixel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Dashboard extends AppCompatActivity {

    // Dummy data until we get API stuff
    private ArrayList<Company> companies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        // Populate dummy data
        companies = new ArrayList<>();
        companies.add(new Company("Apple", "AAPL", 1000.0f));
        companies.add(new Company("Tesla", "TSLA", 800.0f));
        companies.add(new Company("Twitter", "TWTR", 1200.0f));
        companies.add(new Company("Snap", "SNAP", 500.0f));
        companies.add(new Company("XYZ", "XYZ", 1100.0f));

        setupMainChart();

        // setup the list of companies
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        RecyclerView.Adapter mAdapter = new DashboardAdapter(companies);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    // Displays the 'snapshot' of the overall performance of the company
    private void setupMainChart() {
        List<PieEntry> companyEntries = new ArrayList<>();

        for (Company c : companies) {
            companyEntries.add(new PieEntry(c.getValue(), c.getName()));
        }

        PieDataSet dataSet = new PieDataSet(companyEntries, "Snapshot");
        // Currently using the default colour scheme
        dataSet.setColors(ColorTemplate.MATERIAL_COLORS);

        PieData data = new PieData(dataSet);

        // Get the chart
        PieChart mChart = (PieChart) findViewById(R.id.mainChart);
        // Remove the description text
        mChart.getDescription().setEnabled(false);
        // Remove the Legend
        Legend legend = mChart.getLegend();
        legend.setEnabled(false);
        // Create the chart
        mChart.setData(data);
        // Causes a slide in effect when opening the app
        mChart.animateY(1000);
        mChart.invalidate();
    }
}
